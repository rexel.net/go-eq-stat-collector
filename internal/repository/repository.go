package repository

import (
	"database/sql"
	"eq-stat-collector/internal/model"
)

func CreateEvents(db *sql.DB, events []model.Event) error {
	for _, event := range events {
		_, err := db.Exec(
			`INSERT INTO events (event_type, event_time, ticket_number, ticket_time, point_of_sale_id, cash_register_name, cashier_id, client_id, collection_id)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`,
			event.EventType, event.EventTime, event.TicketNumber, event.TicketTime, event.PointOfSaleID, event.CashRegisterName, event.CashierID, event.ClientID, event.CollectionID,
		)
		if err != nil {
			return err
		}
	}

	return nil
}
