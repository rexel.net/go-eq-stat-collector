package middleware

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"eq-stat-collector/internal/model"
)

func ValidateJSON(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Error(w, "Only POST method is allowed", http.StatusMethodNotAllowed)
			return
		}

		var requestBody model.RequestBody
		err := json.NewDecoder(r.Body).Decode(&requestBody)
		if err != nil {
			http.Error(w, "Invalid JSON body", http.StatusBadRequest)
			return
		}

		if len(requestBody.Events) == 0 {
			http.Error(w, "Events array cannot be empty", http.StatusBadRequest)
			return
		}

		for _, event := range requestBody.Events {
			if err := event.Validate(); err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
		}

		// Create a new io.ReadCloser from the requestBody string
		body := io.NopCloser(strings.NewReader(requestBody.String()))

		r.Body = body

		next.ServeHTTP(w, r)
	})
}
