package model

import (
	"encoding/json"
	"fmt"
	"time"
)

type EventType string

const (
	EventTypeClientInvited EventType = "client-invited"
)

type Event struct {
	EventType        EventType `json:"eventType"`
	EventTime        time.Time `json:"eventTime"`
	TicketNumber     string    `json:"ticketNumber"`
	TicketTime       time.Time `json:"ticketTime"`
	PointOfSaleID    int       `json:"pointOfSaleId"`
	CashRegisterName string    `json:"cashRegisterName"`
	CashierID        string    `json:"cashierId"`
	ClientID         string    `json:"clientId"`
	CollectionID     string    `json:"collectionId"`
}

func (e *Event) Validate() error {
	if e.EventType != EventTypeClientInvited {
		return fmt.Errorf("invalid event type: %s", e.EventType)
	}

	if e.EventTime.IsZero() {
		return fmt.Errorf("event time cannot be zero")
	}

	if e.TicketNumber == "" {
		return fmt.Errorf("ticket number cannot be empty")
	}

	if e.TicketTime.IsZero() {
		return fmt.Errorf("ticket time cannot be zero")
	}

	if e.PointOfSaleID <= 0 {
		return fmt.Errorf("point of sale id must be greater than zero")
	}

	if e.CashRegisterName == "" {
		return fmt.Errorf("cash register name cannot be empty")
	}

	if e.CashierID == "" {
		return fmt.Errorf("cashier id cannot be empty")
	}

	if e.ClientID == "" {
		return fmt.Errorf("client id cannot be empty")
	}

	if e.CollectionID == "" {
		return fmt.Errorf("collection id cannot be empty")
	}

	return nil
}

type RequestBody struct {
	Events []Event `json:"events"`
}

func (rb *RequestBody) String() string {
	data, _ := json.Marshal(rb)
	return string(data)
}
