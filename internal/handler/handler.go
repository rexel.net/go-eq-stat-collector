package handler

import (
	"encoding/json"
	"net/http"

	"eq-stat-collector/internal/model"
	"eq-stat-collector/internal/service"
)

func CreateEvents(w http.ResponseWriter, r *http.Request) {
	var requestBody model.RequestBody
	err := json.NewDecoder(r.Body).Decode(&requestBody)
	if err != nil {
		http.Error(w, "Invalid JSON body", http.StatusBadRequest)
		return
	}

	err = service.CreateEvents(&requestBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
