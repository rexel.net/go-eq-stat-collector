package service

import (
	"eq-stat-collector/internal/database"
	"eq-stat-collector/internal/model"
	"eq-stat-collector/internal/repository"
)

func CreateEvents(requestBody *model.RequestBody) error {
	db, err := database.Connect()
	if err != nil {
		return err
	}
	defer db.Close()

	return repository.CreateEvents(db, requestBody.Events)
}
