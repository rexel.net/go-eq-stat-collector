package database

import (
	"database/sql"
)

func Connect() (*sql.DB, error) {
	connStr := "user=postgres password=mysecretpassword dbname=mydb sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
