package main

import (
	"log"
	"net/http"

	"eq-stat-collector/internal/handler"
	"eq-stat-collector/internal/middleware"
)

func main() {
	mux := http.NewServeMux()
	mux.Handle("POST /events", middleware.ValidateJSON(http.HandlerFunc(handler.CreateEvents)))

	log.Println("Server started on :8080")
	log.Fatal(http.ListenAndServe(":8080", mux))
}
